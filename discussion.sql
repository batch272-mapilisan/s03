[SECTION] Inserting Records

-- inserting data to a particular table and 1 column
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("PSY");

-- insterting data to a particular table and/with 2 or more columns
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-Pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ulan", 234, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("214", 279, "OPM", 2);

[SECTION] -- READ AND SELECT RECORDS/DATA

-- Display the title and genre of all songs
SELECT song_name, genre from songs;

-- Display the song name of all the OPM songs
SELECT song_name from songs WHERE genre = "OPM";

-- Display the title of all the songs
SELECT * from songs;

-- Display the title and length of the OPM songs that are more than 2 minutes
SELECT song_name, length FROM songs where genre = "OPM" AND length > 200;

-- Display the title and length of the OPM songs that are more than 2:30 minutes
SELECT song_name, length FROM songs where genre = "OPM" AND length > 230;

[SECTION] Updating Records
UPDATE songs SET length = 259 where song_name = "214";

[SECTION] -- Delete Record
DELETE FROM songs WHERE genre = "OPM" AND length > 250;